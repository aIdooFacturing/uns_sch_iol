package com.unomic.dulink.sch.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.sch.domain.IolVo;
import com.unomic.dulink.sch.service.SchService;
/**
 * Handles requests for the application home page.
 */
@RequestMapping(value = "/sch")
@Controller
public class SchedulerController {

	private static final Logger LOGGER = LoggerFactory.getLogger(SchedulerController.class);
	
	private final String USER_AGENT = "Mozilla/5.0";
	
	@Autowired
	private SchService schService;
	
	@RequestMapping(value="getIol")
	@ResponseBody
	public IolVo getIol(String key){
		CommonCode.MAP_MSG_IOL_IP.get(key);
		String dvcId = key;
		String dvcIp = CommonCode.MAP_MSG_IOL_IP.get(key);
		IolVo rtnVo = schService.getIOLData(dvcId, dvcIp);
		return rtnVo;
	}
	
}

