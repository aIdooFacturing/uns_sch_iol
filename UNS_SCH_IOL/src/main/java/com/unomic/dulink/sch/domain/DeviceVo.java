package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;



@Getter
@Setter
@ToString
public class DeviceVo{

	String dvcId;
	String lastUpdateTime;
	String lastChartStatus;
	String lastStartDateTime;
	String workDate;
}
