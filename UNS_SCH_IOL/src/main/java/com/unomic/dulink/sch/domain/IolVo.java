package com.unomic.dulink.sch.domain;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class IolVo{
	String dvcId;
	String startDateTime;
	String endDateTime;

	String status;
	String chartStatus;

	int iolCycleEnd;
	int iolCycleStart;

	String ip;
	String certKey;
	String workDate;
	
	Boolean ioPower;
	Boolean ioInCycle;
	Boolean ioWait;
	Boolean ioAlarm;
	
	Boolean isSuccess;
	Boolean isDuple;
	Boolean isExist;
	
	String ex1;
	String ex2;
	String ex3;
	
	String strJson;
	int rspnsCode;
	
	public IolVo(){
		this.setIoPower(false);
    	this.setIoInCycle(false);
    	this.setIoAlarm(false);
    	this.setIoWait(false);
	}
}
