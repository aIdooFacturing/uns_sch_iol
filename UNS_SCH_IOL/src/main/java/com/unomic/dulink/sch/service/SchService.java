package com.unomic.dulink.sch.service;

import com.unomic.dulink.sch.domain.IolVo;

public interface SchService {
	
	public IolVo getIOLData(String dvcId, String dvcIp);
}
