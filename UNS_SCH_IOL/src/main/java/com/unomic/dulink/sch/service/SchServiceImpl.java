package com.unomic.dulink.sch.service;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.UnknownHostException;

import javax.annotation.Resource;

import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.gson.Gson;
import com.unomic.dulink.common.domain.CommonCode;
import com.unomic.dulink.common.domain.CommonFunction;
import com.unomic.dulink.sch.domain.DeviceVo;
import com.unomic.dulink.sch.domain.IolVo;

@Service
@Repository
public class SchServiceImpl implements SchService{

	private final static String SCH_SPACE= "com.unos.sch.";
	private final static String IOL_SPACE= "com.unos.iol.";
	
	private final static Logger LOGGER = LoggerFactory.getLogger(SchServiceImpl.class);
	
	@Autowired
	@Resource(name="sqlSession_ma")
	private SqlSession sql_ma;
	
	
	@Override
	public IolVo getIOLData(String dvcId, String dvcIp)
	{
		IolVo rtnVo = new IolVo();
		rtnVo.setDvcId(dvcId);
		rtnVo.setIp(dvcIp);
		
		try {
			URL url = new URL(CommonCode.MSG_HTTP + dvcIp + CommonCode.MSG_IOL_URL);
			URLConnection con = url.openConnection();
			con.setConnectTimeout(CommonCode.CONNECT_TIMEOUT);
			con.setReadTimeout(CommonCode.READ_TIMEOUT);
			InputStream in = con.getInputStream();
			
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String strTemp = org.apache.commons.io.IOUtils.toString(br);
			
			IolVo pureVo = getIOLStatus(strTemp,dvcId);

			rtnVo.setStatus(pureVo.getStatus());
			rtnVo.setStartDateTime(pureVo.getStartDateTime());
			rtnVo.setChartStatus(pureVo.getChartStatus());
			rtnVo.setWorkDate(pureVo.getWorkDate());
		}catch(SocketTimeoutException ex) {
			String exTmp="[DVC_ID:"+dvcId+"@@]"+"IOL NO-CONNECTION:"+"[IP:"+ dvcIp+"]"+ex.toString();
			LOGGER.error(exTmp);
			rtnVo.setEx1(exTmp);
		}catch(UnknownHostException exx){
			String exTmp= "[DVC_ID:"+dvcId+"@@]"+"UnknownHostException Error"+exx.toString();
			LOGGER.error(exTmp);
			rtnVo.setEx2(exTmp);
		}catch(Exception e){
			String exTmp= "[DVC_ID:"+dvcId+"@@]"+"IOL Error"+e.toString();
			LOGGER.error(exTmp);
			rtnVo.setEx3(exTmp);
		}
		
		//String URL="http://www.aidoocontrol.com:8080/UNS_IOL_RCV/mtc/rcvIol.do";
		//rtnVo.setStrJson(URL);
		//rtnVo = setJsonData(rtnVo,URL);
		
		return rtnVo;
	}
	
	
	private IolVo getIpIOL(IolVo inputVo)
	{
		IolVo rtnVo = (IolVo) sql_ma.selectOne(IOL_SPACE + "getIpIOL",inputVo);
		
		return rtnVo;
	}
	
	private int getCntIOL()
	{
		int rtnCnt = (int) sql_ma.selectOne(IOL_SPACE + "getCntIOL");
		
		return rtnCnt;
	}
	
	private IolVo getIOLStatus(String getResult,String dvcId){
		IolVo pureVo = new IolVo();
		
		String[] array;
		array = getResult.split("=|\\<");
		pureVo.setDvcId(dvcId);
		
		if(dvcId.equals("23")||dvcId.equals("24")||dvcId.equals("25")){
			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
		    pureVo.setIoWait((null != array[5] && array[5].equals("1")) ? true : false );
		    pureVo.setIoAlarm((null != array[7] && array[7].equals("1")) ? true : false );
		}else{// origin.
			pureVo.setIoPower((null != array[1] && array[1].equals("1")) ? true : false );
		    pureVo.setIoInCycle((null != array[3] && array[3].equals("1")) ? true : false );
		    pureVo.setIoAlarm((null != array[5] && array[5].equals("1")) ? true : false );
		    pureVo.setIoWait((null != array[7] && array[7].equals("1")) ? true : false );
		}
	    
	    pureVo.setStatus(array[1]+array[3]+array[5]+array[7]);
	    
	    if(pureVo.getIoAlarm()){
	    	pureVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if (pureVo.getIoWait()){
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else if(pureVo.getIoInCycle()){
	    	pureVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }else{
	    	pureVo.setChartStatus(CommonCode.MSG_WAIT);
	    }
	    
	    long crntMillTime = System.currentTimeMillis();
	    
	    pureVo.setStartDateTime(CommonFunction.unixTime2Datetime(crntMillTime));
	    pureVo.setWorkDate(CommonFunction.mil2WorkDate(crntMillTime));
	    
		return pureVo;
	}
	
	private String editLastStatusIOL(DeviceVo inputVo)
	{
		sql_ma.update(IOL_SPACE + "editDvcLastStatusIOL", inputVo);
		return "OK";
	}
	
	
	public IolVo getLastInputData(IolVo inputVo){
		IolVo newVo = new IolVo();
		newVo.setDvcId(inputVo.getDvcId());
		
		Integer cnt =  (Integer) sql_ma.selectOne(IOL_SPACE + "cntAdapterStatus",inputVo);
		
		if(1 < cnt){
			newVo  = (IolVo) sql_ma.selectOne(IOL_SPACE + "getLastAdapterStatus",inputVo);
		}
    	return newVo;
	}

	public IolVo chkIOLStatus(IolVo inputVo){
		String dvcId = inputVo.getDvcId();
		String ioStatus = inputVo.getStatus();
		if(ioStatus == null || ioStatus.length() != CommonCode.IOL_STATUS_LENGTH){
			return inputVo;
		}
		
		//NHM 8000 #1,2,3
		if(dvcId.equals("34")||dvcId.equals("35")||dvcId.equals("40")){
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(2)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(3)=='1') ? true : false);
		}else{
			inputVo.setIoPower((ioStatus.charAt(0)=='1') ? true : false);
			inputVo.setIoInCycle((ioStatus.charAt(1)=='1') ? true : false);
			inputVo.setIoAlarm((ioStatus.charAt(2)=='1') ? true : false);
			inputVo.setIoWait((ioStatus.charAt(3)=='1') ? true : false);
		}
	    if(inputVo.getIoAlarm()){
	    	inputVo.setChartStatus(CommonCode.MSG_ALARM);
	    }else if(inputVo.getIoWait()){
	    	inputVo.setChartStatus(CommonCode.MSG_WAIT);
	    }else{
	    	inputVo.setChartStatus(CommonCode.MSG_IN_CYCLE);
	    }
		return inputVo;
	}

	public IolVo chkDateStarterIOL(IolVo preVo, IolVo crtVo){
		if(CommonFunction.isDateStart(preVo.getStartDateTime(), crtVo.getStartDateTime())){
			LOGGER.error("@@@@@RUN DateStarter IOL@@@@@");
			LOGGER.error("startDateTime:"+preVo.getStartDateTime());
			LOGGER.error("endDateTime:"+crtVo.getStartDateTime());
			
			IolVo starterVo = new IolVo();
			starterVo.setDvcId(crtVo.getDvcId());
			starterVo.setStartDateTime(CommonFunction.getStandardHourToday());
			starterVo.setEndDateTime(CommonFunction.getStandardP1SecToday());
			starterVo.setChartStatus(CommonCode.MSG_DATE_START);
			starterVo.setWorkDate(CommonFunction.dateTime2WorkDate(CommonFunction.getStandardHourToday()));
			//list.add(i, starterVo);
			editLastEndTime(starterVo);
			addIOLStatus(starterVo);
	
			return starterVo;
		}
		return null;
	}

	
	@Transactional
	private String editLastEndTime(IolVo firstOfListVo)
	{
		LOGGER.info("editLastEndTime");
		int cnt = (int) sql_ma.selectOne(IOL_SPACE + "cntAdapterStatus", firstOfListVo);
		if(cnt < 1){
			return "OK";
		}
		
		IolVo tmpAdapterVo = (IolVo) sql_ma.selectOne(IOL_SPACE + "getLastStartTime", firstOfListVo);
		if(tmpAdapterVo != null){
			sql_ma.delete(IOL_SPACE + "rmExceptionEndTime",tmpAdapterVo);
		}
		tmpAdapterVo.setEndDateTime(firstOfListVo.getStartDateTime());
		sql_ma.update(IOL_SPACE + "editLastEndTime", tmpAdapterVo);
		
		return "OK";
	}
	
	@Transactional
	private String addIOLStatus(IolVo pureStatusVo)
	{
		sql_ma.insert(IOL_SPACE+"addIOLData", pureStatusVo);
		return "OK";
	}
	
	public IolVo isDupleIOL(IolVo preIOLVo, IolVo inputVo){
		Long preStartTime = 0L;
		if(null == preIOLVo.getStartDateTime()){
			preStartTime = CommonFunction.dateTime2Mil(preIOLVo.getStartDateTime());
		}else{
			preStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		}
		Long crtStartTime = CommonFunction.dateTime2Mil(inputVo.getStartDateTime());
		if(crtStartTime < preStartTime){
			LOGGER.error("timeTrash data");
			return null;
		}
    	if( preIOLVo.getStatus() != null && preIOLVo.getStatus().equals(inputVo.getStatus())){
    		
    	    	return null;
    	}else{
    		return inputVo;
    	}
	}
	
	private IolVo setJsonData(IolVo inputVo,String url){
		String strStep = "0";
		String USER_AGENT = "Mozilla/5.0";
		LOGGER.error("RUN setJsonData");
		URL obj;
		int responseCode =0;
		
		strStep += "1";
		try {
			obj = new URL(url);

			HttpURLConnection con = (HttpURLConnection) obj.openConnection();
			//add reuqest header
			con.setRequestMethod("POST");
			con.setRequestProperty("User-Agent", USER_AGENT);
			con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
	
			// Send post request
			con.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(con.getOutputStream());
			
			Gson gson = new Gson();
			String strJson = gson.toJson(inputVo);
			
			inputVo.setStrJson(strJson);
			wr.writeBytes(strJson);
			wr.flush();
			wr.close();
			responseCode = con.getResponseCode();
			inputVo.setRspnsCode(responseCode);
			BufferedReader in = new BufferedReader(
			        new InputStreamReader(con.getInputStream()));
			String inputLine;
			StringBuffer response = new StringBuffer();

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
	
		}catch(Exception e){
			e.printStackTrace();
		}
		return inputVo;
	}
	
}