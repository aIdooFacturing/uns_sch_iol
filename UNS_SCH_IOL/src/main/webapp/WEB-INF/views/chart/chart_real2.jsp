<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include/lib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="chrome=1">
<meta http-equiv="cache-control" content="max-age=0" />
<meta http-equiv="cache-control" content="no-cache" />
<meta http-equiv="expires" content="0" />
<meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
<meta http-equiv="pragma" content="no-cache" />
<link rel="shortcut icon" href="${ctxPath }/images/dulink_favicon.ico">

<title>Dash Board</title>
<script type="text/javascript">
	var ctxPath = "${ctxPath}";
	var targetWidth = 3840;
	var targetHeight = 2160;

	var originWidth = window.innerWidth;
	var originHeight = window.innerHeight;

	var contentWidth = originWidth;
	var contentHeight = targetHeight / (targetWidth / originWidth);

	var screen_ratio = getElSize(240);

	if (originHeight / screen_ratio < 9) {
		contentWidth = targetWidth / (targetHeight / originHeight)
		contentHeight = originHeight;
	};

	var marginWidth = (originWidth-contentWidth)/2;
	var marginHeight = (originHeight-contentHeight)/2;
	
	function getElSize(n) {
		return contentWidth / (targetWidth / n);
	};

	function setElSize(n) {
		return targetWidth / (contentWidth / n);
	};
</script>
<script type="text/javascript" src="${ctxPath }/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery-ui.js"></script>
<script type="text/javascript" src="${ctxPath }/js/jquery.circle-diagram.js"></script>
<script src="${ctxPath }/js/jqMap.js"></script>
<script src="${ctxPath }/js/chart/highstock_data.js"></script>	
<script type="text/javascript" src="${ctxPath }/js/highstock.js"></script>
<script type="text/javascript" src="${ctxPath }/js/highcharts-3d.js"></script>
<script src="${ctxPath }/js/multicolor_series.js"></script>
<script src="${ctxPath }/js/card_real.js"></script>
<link rel="stylesheet" href="${ctxPath }/css/card.css">
</head>

<body oncontextmenu="setSlideMode(); return false">
	<div id="panel">
		<table id="panel_table" width="100%">
			<tr align="center">
				<td colspan="2"><p id="comName">MENU</p></td>
			</tr>
			<tr align="center">
				<td width="50%">
					<img alt="" src="${ctxPath }/images/menu/board.png" class="menu_icon" id="DashBoard_1" draggable="false">
					<p>DashBoard</p>
				</td>
				<td>
					<img alt="" src="${ctxPath }/images/menu/report.png" class="menu_icon" id="Report_3" draggable="false">
					<p>Report</p>
				</td>
			</tr>
			
		</table>
	</div>
	<div id="corver"></div>
	<img alt="" src="${ctxPath }/images/myApps.png" id="menu_btn">
	
	<!-- Part1 -->
	<div id="part1" class="page">
		<div class="container">
		<canvas id="canvas" width="200" height="200"></canvas>
		<div id="stateBorder"></div>
			<img alt="" src="${ctxPath }/images/map.svg" id="map">
			<center>
				<table class="mainTable" id="mainTable">
					<tr>
						<Td align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder; border-radius : 15px;" class="title" >
							<%-- <img alt="" src="${ctxPath }/images/loginForm/Factory911_3.png"  class="title_left"> --%>
								DashBoard
							<%-- <img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right"> --%>
						</Td>
					</tr>
				</table>
				
				<div id="cards">
				
				</div>
				<div id="svg">
			
				</div>
			</center>
		</div>
	</div>
	
	<!--Part2  -->
	<div id="part2" class="page">
		<div id="mainTable2">
			<center>
				<table class="mainTable" >
					<tr>
						<Td align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="title" colspan="4">
							<img alt="" src="${ctxPath }/images/loginForm/Factory911_3.png" class="title_left">
								개별 장비 상태
							<img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right">
						</Td>
					</tr>
					<Tr class="tr1" >
						<td colspan="3" width="60%" rowspan="2" valign="middle">
						<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Status	
							</div>
							<div id="container"></div>
						</td>
						<td width="40%" valign="top" id="machine_name_td">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Machine	
							</div>
							<div id="machine_name">
								
							</div>
						</td>		
					</Tr >
					<tr class="tr1">
						<td valign="top">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Alarm	
							</div>
							<div id="alarm" align="left"></div>
						</td>
						
					</tr>
					<tr class="tr2" valign="top" id="chartTr">
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Operation time Ratio				
							</div>	
							<div class="neon1">
							 	88
							 </div>
						</td>
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
								Motor End_up Movement		
							</div>
							<div class="neon2">
							04
							</div>
						</td>
						<td width="20%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Motor Up_Down Movement				
							</div>
							<div class="neon3" >
							22
							</div>
						</td>
						<Td width="40%">
							<div align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="subTitle">
							Daily Status				
							</div>
							<div id="pie" style="width: 100%"></div>
						</Td>
					</tr>
				</table>
			</center> 
		</div>
	</div>
	
	<!-- Part3 -->
	<div id="part3" class="page" style="background-color: rgb(234,234,234)">
		<center>
			<table class="mainTable" id="reportTable"style="background-color: rgb(234,234,234)" >
				<tr>
					<Td align="center" style="background-color:rgb(239,174,62); color:white; font-weight: bolder;" class="title" colspan="5">
						<img alt="" src="${ctxPath }/images/loginForm/Factory911_3.png"  class="title_left">
							통계
						<img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right">
					</Td>
				</tr>
				<tr >
					<td align="center" colspan="5">
						<div class="td_header">
							<div id="reportDateDiv">
								<input type="date" id="sDate"> - 
								<input type="date" id="eDate">
							</div>
							Status
						</div>
						<div id="columnChart"></div>
					</td>
				</tr>
				<tr>
					<td width="25%" align="center" valign="top">
						<div class="td_header">
							OEE improvement ratio
						</div>
						<span class="span" id="lineChartLabel"></span>
						<div id="lineChart"></div>
					</td>
					<td width="25%" align="center" valign="top">
						<div class="td_header">
							In-Cycle Time
						</div>
						<span class="upDownSpan span" id="incycleTime_avg">4,671</span>
						<div>
							<%-- <img alt="" src="${ctxPath }/images/up.png" class="upDown"><font id="upFont">15.5%</font> --%>
						</div>
					</td>
					<td width="25%" align="center" valign="top">
						<div class="td_header">
							Down Time
						</div>
						<span class="upDownSpan span" id="alarmTime_avg">4,418</span>
						<div>
							<%-- <img alt="" src="${ctxPath }/images/down.png" class="upDown" ><font id="downFont">5.8%</font> --%>
						</div>
					</td>
					<td width="25%" align="center" valign="top" id="diagramTd">
						<div class="td_header">
							Operation Time Ratio
						</div>
						<div id="diagram" ></div>
					</td>
				</tr>
			</table>
		</center>
	</div>
	
	<!-- Part4 -->
	<div id="part4" class="page">
		<center>
			<table class="mainTable">
				<tr>
					<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="2">
						<img alt="" src="${ctxPath }/images/loginForm/Factory911_2.png"  class="title_left">
							Report
						<img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right">
					</Td>
				</tr>
			</table>
		</center>
	</div>
	
	<!-- Part5 -->
	<div id="part5" class="page">
		<center>
			<table class="mainTable">
				<tr>
					<Td align="center" style="background-color:rgb(34,34,34); color:white; font-weight: bolder;" class="title" colspan="2">
						<img alt="" src="${ctxPath }/images/loginForm/Factory911_2.png"  class="title_left">
							Settings
						<img alt="" src="${ctxPath }/images/loginForm/SmartFactory2.png" class="title_right">
					</Td>
				</tr>
			</table>
		</center>
	</div>
</body>
</html>